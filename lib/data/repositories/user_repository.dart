import 'package:sqflite/sqflite.dart';
import 'package:test_case/data/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_case/data/repositories/database_repository.dart';

class UserRepository {
  final dbProvider = DatabaseRepository.instance;

  Future<List<User>> showAll() async {
    final db = await dbProvider.database;
    final List<Map<String, dynamic>> maps = await db.query("user");
    List<User> users = maps.map((item) => User.fromMap(item)).toList();
    return users;
  }

  Future<User> insert(User user) async {
    final db = await dbProvider.database;
    user.id = await db.insert(
      'user',
      user.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return user;
  }

  Future<void> update(User user) async {
    final db = await dbProvider.database;
    await db.update(
      'user',
      user.toMap(),
      where: "id = ?",
      whereArgs: [user.id],
    );
  }

  Future<void> delete(int id) async {
    final db = await dbProvider.database;
    await db.delete(
      'user',
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future<User> getUser(int id) async {
    final db = await dbProvider.database;
    List<Map> maps = await db.query('user', where: 'id = ?', whereArgs: [id]);
    if (maps.length > 0) {
      return User.fromMap(maps.first);
    }
    return null;
  }

  Future<User> getUserByUsername(String username, String password) async {
    final db = await dbProvider.database;
    List<Map<String, dynamic>> maps = await db.query('user',
        where: 'username = ? and password = ?',
        whereArgs: [username, password]);
    if (maps.toList().length > 0) {
      return User.fromMap(maps.first);
    }
    return null;
  }

  Future close() async {
    final db = await dbProvider.database;
    db.close();
  }

  // shared pref
  Future<void> persistToken(String token) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('user', token);
    return;
  }

  Future<void> deleteToken() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('user');
    return;
  }

  Future<bool> hasToken() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString('user') == null ? false : true;
  }

  Future<String> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString('user');
  }
}
