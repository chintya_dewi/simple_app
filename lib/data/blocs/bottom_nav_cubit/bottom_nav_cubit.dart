import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

part 'bottom_nav_state.dart';

class BottomNavCubit extends Cubit<int> {
  BottomNavCubit() : super(0);

  void navItemTapped(int index) {
    emit(index);
  }

  List<BottomNavItem> get navItem {
    return [
      BottomNavItem(
        icon: Icon(
          Icons.home_outlined,
        ),
        activeIcon: Icon(
          Icons.home,
        ),
        label: "Home",
      ),
      BottomNavItem(
        icon: Icon(Icons.favorite_outline),
        activeIcon: Icon(
          Icons.favorite,
        ),
        label: "Favorite",
      ),
      BottomNavItem(
        icon: Icon(
          Icons.person_outline,
        ),
        activeIcon: Icon(
          Icons.person,
        ),
        label: "Profile",
      ),
    ];
  }
}

class BottomNavItem {
  BottomNavItem({
    @required this.icon,
    @required this.activeIcon,
    @required this.label,
    this.onTaped,
  });

  final Widget icon;
  final Widget activeIcon;
  final String label;
  final VoidCallback onTaped;
}
