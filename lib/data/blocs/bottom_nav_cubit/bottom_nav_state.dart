part of 'bottom_nav_cubit.dart';

class BottomNavState extends Equatable {
  BottomNavState({@required this.index});

  final int index;

  @override
  List<Object> get props => [index];
}