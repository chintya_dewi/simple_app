part of 'delete_people_cubit.dart';

abstract class DeletePeopleState extends Equatable {
  const DeletePeopleState();

  @override
  List<Object> get props => [];
}

class DeletePeopleInitial extends DeletePeopleState {}

class DeletePeopleLoading extends DeletePeopleState {}

class DeletePeopleSuccess extends DeletePeopleState {}

class DeletePeopleFailure extends DeletePeopleState {
  final String message;

  DeletePeopleFailure({this.message});

  @override
  List<Object> get props => [message];
}