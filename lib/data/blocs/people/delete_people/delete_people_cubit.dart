import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:test_case/data/repositories/people_repository.dart';

part 'delete_people_state.dart';

class DeletePeopleCubit extends Cubit<DeletePeopleState> {
  DeletePeopleCubit() : super(DeletePeopleInitial());
  
  final PeopleRepository _peopleRepository = PeopleRepository();

  Future<void> delete({@required int id}) async {
    emit(DeletePeopleLoading());
    try {
      await _peopleRepository.delete(id);
      emit(DeletePeopleSuccess());
    } catch (error) {
      emit(DeletePeopleFailure(message: error.toString()));
    }
  }
}
