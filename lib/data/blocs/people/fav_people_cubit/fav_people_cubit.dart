import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_case/data/models/people.dart';
import 'package:test_case/data/repositories/people_repository.dart';

part 'fav_people_state.dart';

class FavPeopleCubit extends Cubit<FavPeopleState> {
  FavPeopleCubit() : super(FavPeopleInitial());

  final PeopleRepository _peopleRepository = PeopleRepository();

  Future<void> showAllFavData() async {
    emit(FavPeopleLoading());
    try {
      final userDataLocal = await _peopleRepository.showAllFav();
      emit(FavPeopleLoaded(userDataLocal));
    } catch (error) {
      emit(FavPeopleLoadFailure(message: error.toString()));
    }
  }

  Future<void> reloadFavLocalData() async {
    try {
      final userDataLocal = await _peopleRepository.showAllFav();
      emit(FavPeopleLoaded(userDataLocal));
    } catch (error) {
      emit(FavPeopleLoadFailure(message: error.toString()));
    }
  }

  Future<void> searchFav(String keyword) async {
    emit(FavPeopleLoading());
    try {
      final userDataLocal = await _peopleRepository.searchPeopleFav(keyword);
      emit(FavPeopleLoaded(userDataLocal));
    } catch (error) {
      emit(FavPeopleLoadFailure(message: error.toString()));
    }
  }
}
