part of 'fav_people_cubit.dart';

abstract class FavPeopleState extends Equatable {
  const FavPeopleState();

  @override
  List<Object> get props => [];
}

class FavPeopleInitial extends FavPeopleState {}

class FavPeopleLoading extends FavPeopleState {}

class FavPeopleLoaded extends FavPeopleState {
  FavPeopleLoaded(this.people);

  final List<People> people;

  @override
  List<Object> get props => [people];
}

class FavPeopleLoadFailure extends FavPeopleState {
  final String message;

  FavPeopleLoadFailure({this.message});

  @override
  List<Object> get props => [message];
}
