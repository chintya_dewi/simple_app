part of 'set_fav_people_cubit.dart';

abstract class SetFavPeopleState extends Equatable {
  const SetFavPeopleState();

  @override
  List<Object> get props => [];
}

class SetFavPeopleInitial extends SetFavPeopleState {}

class SetFavPeopleLoading extends SetFavPeopleState {}

class SetFavPeopleSuccess extends SetFavPeopleState {}

class SetFavPeopleFailure extends SetFavPeopleState {
  final String message;

  SetFavPeopleFailure({this.message});

  @override
  List<Object> get props => [message];
}
