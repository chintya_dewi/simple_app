part of 'people_cubit.dart';

abstract class PeopleState extends Equatable {
  @override
  List<Object> get props => [];
}

class PeopleInitial extends PeopleState {}

class PeopleLoading extends PeopleState {}

class PeopleLoaded extends PeopleState {
  PeopleLoaded(this.people, this.view, this.sort);

  final List<People> people;
  final ViewPeople view;
  final SortPeople sort;

  @override
  List<Object> get props => [people, view, sort];
}

class PeopleLoadFailure extends PeopleState {
  final String message;

  PeopleLoadFailure({this.message});

  @override
  List<Object> get props => [message];
}