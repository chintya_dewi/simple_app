part of 'people_entry_cubit.dart';

abstract class PeopleEntryState extends Equatable {
  const PeopleEntryState();

  @override
  List<Object> get props => [];
}

class PeopleEntryInitial extends PeopleEntryState {}

class PeopleEntryLoading extends PeopleEntryState {}

class PeopleEntrySuccess extends PeopleEntryState {}

class PeopleEntryFailure extends PeopleEntryState {
  final String message;

  PeopleEntryFailure({this.message});

  @override
  List<Object> get props => [message];
}
