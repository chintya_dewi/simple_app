import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:test_case/data/models/user.dart';
import 'package:test_case/data/repositories/user_repository.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(AuthInitial());
  UserRepository _userRepository = UserRepository();

  Future<void> appStarted() async {
    final bool hasToken = await _userRepository.hasToken();
    emit(AuthLoading());
    if (hasToken) {
      try {
        final int id = int.parse(await _userRepository.getToken());
        final User user = await _userRepository.getUser(id);
        if (user != null) {
          emit(AuthAuthenticated(user: user));
        } else {
          emit(
            AuthUnauthenticated(),
          );
        }
      } catch (error) {
        emit(
          AuthUnauthenticated(
            message: error.toString(),
          ),
        );
      }
    } else {
      emit(
        AuthUnauthenticated(),
      );
    }
  }

  Future<void> login({
    @required String username,
    @required String password,
  }) async {
    emit(AuthLoading());
    try {
      final User user =
          await _userRepository.getUserByUsername(username, password);
      if (user != null) {
        await _userRepository.persistToken(user.id.toString());
        emit(AuthAuthenticated(user: user));
      } else {
        emit(
          AuthUnauthenticated(
            message: "Register first",
          ),
        );
      }
    } catch (error) {
      emit(
        AuthUnauthenticated(
          message: error.toString('Register first'),
        ),
      );
    }
  }

  Future<void> register(
      {@required String name,
      @required String username,
      @required String password}) async {
    emit(AuthLoading());
    try {
      final User userExist =
          await _userRepository.getUserByUsername(username, password);
      if (userExist == null) {
        final User newUser = await _userRepository.insert(
          User(name: name, username: username, password: password),
        );
        final User user = await _userRepository.getUser(newUser.id);
        _userRepository.persistToken(newUser.id.toString());
        emit(AuthAuthenticated(user: user));
      } else {
        emit(
          AuthUnauthenticated(
            message: "User already exist",
          ),
        );
      }
    } catch (error) {
      emit(
        AuthUnauthenticated(
          message: error.toString(),
        ),
      );
    }
  }

  Future<void> logout() async {
    await _userRepository.deleteToken();
    emit(
      AuthUnauthenticated(),
    );
  }
}
