export 'api_exceptions.dart';
export 'api_provider.dart';

enum ErrorType { network, client, server, general }
