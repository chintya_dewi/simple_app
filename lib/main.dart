import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_case/data/blocs/auth_cubit/auth_cubit.dart';
import 'package:test_case/data/blocs/bottom_nav_cubit/bottom_nav_cubit.dart';
import 'package:test_case/data/blocs/people/fav_people_cubit/fav_people_cubit.dart';
import 'package:test_case/data/blocs/people/people_cubit/people_cubit.dart';
import 'package:test_case/ui/screens/screens.dart';
import 'package:test_case/utils/transitions.dart' as AppTrans;
import 'package:test_case/utils/colors.dart' as AppColor;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.dark),
  );

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  AuthCubit authCubit;
  BottomNavCubit bottomNavCubit;
  PeopleCubit peopleCubit;
  FavPeopleCubit favPeopleCubit;

  @override
  void initState() {
    super.initState();
    authCubit = AuthCubit()..appStarted();
    bottomNavCubit = BottomNavCubit();
    peopleCubit = PeopleCubit();
    favPeopleCubit = FavPeopleCubit();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => authCubit),
        BlocProvider(create: (_) => bottomNavCubit),
        BlocProvider(create: (_) => peopleCubit),
        BlocProvider(create: (_) => favPeopleCubit),
      ],
      child: MaterialApp(
        title: "MStar",
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blueGrey,
          primaryColor: AppColor.primary,
          primaryColorBrightness: Brightness.light,
          scaffoldBackgroundColor: Colors.white,
          textSelectionTheme:
              TextSelectionThemeData(cursorColor: AppColor.editTextCursor),
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: BlocBuilder(
          cubit: authCubit,
          builder: (context, state) => AppTrans.FadeThroughTransitionSwitcher(
            fillColor: Colors.transparent,
            child: state is AuthInitial
                ? SplashScreen()
                : state is AuthAuthenticated
                    ? MainScreen()
                    : state is AuthUnauthenticated
                        ? LoginScreen()
                        : SplashScreen(),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    authCubit.close();
    bottomNavCubit.close();
    peopleCubit.close();
    super.dispose();
  }
}
