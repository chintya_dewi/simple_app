import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:test_case/utils/extensions.dart' as AppExt;
import 'package:test_case/utils/colors.dart' as AppColor;
import 'package:test_case/utils/transitions.dart' as AppTrans;

class RoundedButton extends StatelessWidget {
  final String label;
  final VoidCallback onPressed;
  final Color color;
  final bool _isContained;
  final Widget leading;
  final bool isSmall;

  RoundedButton.contained({
    Key key,
    this.label,
    @required this.onPressed,
    this.color,
    this.leading,
    this.isSmall = false,
  })  : this._isContained = true,
        super(key: key);

  RoundedButton.outlined({
    Key key,
    this.label,
    @required this.onPressed,
    this.color,
    this.leading,
    this.isSmall = false,
  })  : this._isContained = false,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final Color buildColor = color ?? AppColor.primary;

    return _isContained
        ? buildContained(buildColor)
        : buildOutlined(buildColor);
  }

  ElevatedButton buildContained(Color buildColor) {
    return ElevatedButton(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled))
              return AppColor.roundedButtonDisabled;
            return buildColor;
          },
        ),
        foregroundColor: MaterialStateProperty.resolveWith(
          (states) {
            if (states.contains(MaterialState.disabled))
              return AppColor.textSecondary;
            return AppExt.computeTextColor(buildColor);
          },
        ),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        ),
        padding: MaterialStateProperty.all(
          EdgeInsets.all(2),
        ),
        shadowColor: MaterialStateProperty.resolveWith(
          (states) {
            return Colors.black38;
          },
        ),
        elevation: MaterialStateProperty.resolveWith(
          (states) {
            if (states.contains(MaterialState.disabled)) return 0;
            if (states.contains(MaterialState.pressed)) return 1;
            return 1;
          },
        ),
      ),
      onPressed: onPressed,
      child: Container(
        margin: EdgeInsets.all(isSmall ? 5 : 10),
        child: Center(
            child: AppTrans.SharedAxisTransitionSwitcher(
          transitionType: SharedAxisTransitionType.vertical,
          fillColor: Colors.transparent,
          child: Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              leading != null ? leading : SizedBox.shrink(),
              leading != null && label != null
                  ? SizedBox(
                      width: 5,
                    )
                  : SizedBox.shrink(),
              label != null
                  ? Text(
                      label,
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: onPressed == null
                              ? FontWeight.w500
                              : FontWeight.w700),
                      textAlign: TextAlign.center,
                    )
                  : SizedBox.shrink(),
            ],
          ),
        )),
      ),
    );
  }

  Widget buildOutlined(Color buildColor) {
    return OutlinedButton(
      onPressed: onPressed,
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            if (states.contains(MaterialState.disabled))
              return AppColor.roundedButtonDisabled;
            return Colors.white;
          },
        ),
        overlayColor: MaterialStateProperty.resolveWith(
          (states) {
            if (states.contains(MaterialState.hovered))
              return buildColor.withOpacity(0.1);
            if (states.contains(MaterialState.pressed))
              return buildColor.withOpacity(0.1);
            return Colors.transparent;
          },
        ),
        foregroundColor: MaterialStateProperty.resolveWith(
          (states) {
            if (states.contains(MaterialState.disabled))
              return AppColor.textSecondary;
            return AppExt.computeTextColor(buildColor);
          },
        ),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
        ),
        padding: MaterialStateProperty.all(
          EdgeInsets.all(2),
        ),
        shadowColor: MaterialStateProperty.resolveWith(
          (states) {
            return Colors.black38;
          },
        ),
        side: MaterialStateProperty.resolveWith((states) {
          if (states.contains(MaterialState.disabled))
            return BorderSide(color: AppColor.roundedButtonDisabled, width: 2);
          return BorderSide(color: buildColor, width: 2);
        }),
        elevation: MaterialStateProperty.resolveWith(
          (states) {
            if (states.contains(MaterialState.disabled)) return 0;
            if (states.contains(MaterialState.pressed)) return 1;
            return 1;
          },
        ),
      ),
      child: Container(
        margin: EdgeInsets.all(isSmall ? 5 : 10),
        child: Center(
          child: AppTrans.SharedAxisTransitionSwitcher(
            transitionType: SharedAxisTransitionType.vertical,
            fillColor: Colors.transparent,
            child: Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                leading != null ? leading : SizedBox.shrink(),
                leading != null && label != null
                    ? SizedBox(
                        width: 5,
                      )
                    : SizedBox.shrink(),
                label != null
                    ? Text(
                        label,
                        style: TextStyle(
                          color:
                              onPressed == null ? Colors.grey[400] : buildColor,
                          fontSize: 16,
                          fontWeight: onPressed == null
                              ? FontWeight.w500
                              : FontWeight.w700,
                        ),
                        textAlign: TextAlign.center,
                      )
                    : SizedBox.shrink(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
