import 'package:flutter/material.dart';
import 'package:test_case/ui/widgets/rounded_button.dart';
import 'package:test_case/utils/extensions.dart' as AppExt;
import 'package:test_case/utils/colors.dart' as AppColor;

class ConfirmationDialog {
  static Future show(BuildContext context,
      {@required String desc, @required Function() onYes}) async {
    AppExt.hideKeyboard(context);
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10)), //this right here
            child: Container(
              child: Padding(
                padding: const EdgeInsets.all(25.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      desc ?? '',
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: RoundedButton.contained(
                            label: "Yes",
                            onPressed: () {
                              Navigator.pop(context);
                              onYes();
                            },
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          child: RoundedButton.outlined(
                            label: "No",
                            onPressed: () => Navigator.pop(context),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        });
    return;
  }
}
