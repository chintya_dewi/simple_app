import 'package:flutter/material.dart';
import 'package:test_case/data/models/people.dart';
import 'package:test_case/ui/screens/screens.dart';
import 'package:test_case/utils/extensions.dart' as AppExt;
import 'package:test_case/utils/colors.dart' as AppColor;

class CardPeople extends StatelessWidget {
  const CardPeople(
      {Key key,
      @required this.people,
      this.isGrid = false,
      @required this.setFavorite})
      : super(key: key);
  final People people;
  final bool isGrid;
  final Function setFavorite;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(5),
        ),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: AppColor.black.withOpacity(0.05),
            blurRadius: 5,
            offset: Offset(0, 0),
          ),
        ],
      ),
      child: Material(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(5),
        ),
        elevation: 0,
        child: InkWell(
          onTap: () => AppExt.pushScreen(
            context,
            PeopleDetailScreen(people: people),
          ),
          child: Padding(
            padding:
                EdgeInsets.fromLTRB(20, isGrid ? 20 : 12, 20, isGrid ? 0 : 12),
            child: !isGrid
                ? Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "${people.name.capitalize()}",
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w700),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "${people.gender.capitalize()}",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: AppColor.textSecondary,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w700),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "Height: ${people.height} - Mass: ${people.mass}",
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: AppColor.textSecondary, fontSize: 14),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 5),
                      IconButton(
                        padding: EdgeInsets.zero,
                        iconSize: 22,
                        splashRadius: 25,
                        icon: Icon(
                            people.favorite == 0
                                ? Icons.favorite_outline
                                : Icons.favorite,
                            color: AppColor.red),
                        onPressed: () => setFavorite(),
                      ),
                    ],
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "${people.name.capitalize()}",
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w700),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "${people.gender.capitalize()}",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: AppColor.textSecondary,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w700),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "Height: ${people.height} - Mass: ${people.mass}",
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: AppColor.textSecondary, fontSize: 14),
                            ),
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: IconButton(
                          padding: EdgeInsets.zero,
                          iconSize: 22,
                          splashRadius: 25,
                          icon: Icon(
                              people.favorite == 0
                                  ? Icons.favorite_outline
                                  : Icons.favorite,
                              color: AppColor.red),
                          onPressed: () => setFavorite(),
                        ),
                      )
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}
