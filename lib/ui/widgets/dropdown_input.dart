import 'package:flutter/material.dart';
import 'package:test_case/utils/colors.dart' as AppColor;

class DropdownInput extends StatelessWidget {
  const DropdownInput({
    Key key,
    @required this.isEmpty,
    @required this.child,
    this.fillColor,
    this.hintText,
  }) : super(key: key);
  final bool isEmpty;
  final Color fillColor;
  final Widget child;
  final String hintText;

  @override
  Widget build(BuildContext context) {
    return InputDecorator(
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(15, 10, 15, 10),
        hintText: hintText ?? '',
        filled: true,
        fillColor: fillColor ?? AppColor.editTextField,
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
        ),
      ),
      isEmpty: isEmpty,
      child: DropdownButtonHideUnderline(
        child: child,
      ),
    );
  }
}
