import 'package:flutter/material.dart';
import 'package:test_case/utils/extensions.dart' as AppExt;
import 'package:test_case/utils/colors.dart' as AppColor;

class BottomSheetFeedback {
  const BottomSheetFeedback();

  static Future show(
    BuildContext context, {
    @required String title,
    String description,
    IconData icon,
    Color color,
  }) async {
    AppExt.hideKeyboard(context);
    await showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(15))),
        context: context,
        builder: (BuildContext bc) {
          return Padding(
            padding: EdgeInsets.all(15),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 50,
                  height: 5,
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.15),
                    borderRadius: BorderRadius.circular(7.5 / 2),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                ListView(
                  shrinkWrap: true,
                  children: [
                    Icon(
                      icon ?? Icons.check_circle_outline,
                      color: color ?? AppColor.green,
                      size: 55,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      title ?? '',
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                    ),
                    Text(
                      description ?? '',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 14, color: AppColor.textSecondary),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ],
            ),
          );
        });
    return;
  }
}
