import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_case/utils/colors.dart' as AppColor;

enum InputType { text, password, search, option, field }

class EditText extends StatefulWidget {
  final String hintText;
  final String initialValue;
  final InputType inputType;
  final TextEditingController controller;
  final Function onChanged;
  final Function onFieldSubmitted;
  final Function validator;
  final AutovalidateMode autoValidateMode;
  final TextInputType keyboardType;
  final void Function() onTap;
  final bool readOnly;
  final bool isAlwaysBordered;
  final EdgeInsets padding;
  final int maxLength;
  final List<TextInputFormatter> inputFormatter;
  final FocusNode focusNode;

  const EditText({
    Key key,
    @required this.hintText,
    this.inputType = InputType.text,
    this.controller,
    this.onChanged,
    this.keyboardType,
    this.onTap,
    this.readOnly = false,
    this.isAlwaysBordered = false,
    this.padding,
    this.maxLength,
    this.inputFormatter,
    this.onFieldSubmitted,
    this.initialValue,
    this.focusNode,
    this.validator,
    this.autoValidateMode = AutovalidateMode.disabled,
  }) : super(key: key);

  @override
  _EditTextState createState() => _EditTextState();
}

class _EditTextState extends State<EditText> {
  bool _obscureText;
  FocusNode _focus = new FocusNode();

  @override
  void initState() {
    super.initState();
    _obscureText = widget.inputType == InputType.password ? true : false;
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onTap: widget.onTap ?? null,
      readOnly: widget.readOnly == true,
      focusNode: widget.focusNode ?? _focus,
      textAlign: TextAlign.start,
      maxLength: widget.maxLength,
      maxLines: widget.inputType == InputType.field ? 5 : 1,
      keyboardType: widget.keyboardType,
      onChanged: widget.onChanged,
      autovalidateMode: widget.autoValidateMode,
      onFieldSubmitted: widget.onFieldSubmitted,
      validator: widget.validator,
      initialValue: widget.initialValue,
      controller: widget.controller,
      obscureText: _obscureText,
      decoration: InputDecoration(
          counter: SizedBox.shrink(),
          contentPadding: widget.padding ??
              EdgeInsets.fromLTRB(
                  15,
                  10,
                  widget.inputType == InputType.password ||
                          widget.inputType == InputType.search
                      ? 4
                      : 15,
                  10),
          hintText: _focus.hasFocus ? null : widget.hintText,
          filled: true,
          fillColor: widget.isAlwaysBordered == true
              ? Colors.transparent
              : AppColor.editTextField,
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: widget.isAlwaysBordered == true
                ? BorderSide(color: AppColor.grey, width: 1)
                : BorderSide(color: AppColor.editText, width: 0.0),
          ),
          enabledBorder: widget.inputType == InputType.search
              ? UnderlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: widget.isAlwaysBordered == true
                      ? BorderSide(color: AppColor.grey, width: 1)
                      : BorderSide(color: AppColor.editText, width: 0.0),
                )
              : OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: widget.isAlwaysBordered == true
                      ? BorderSide(color: AppColor.grey, width: 1)
                      : BorderSide(color: AppColor.editText, width: 0.0),
                ),
          focusedBorder: widget.inputType == InputType.search
              ? UnderlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: widget.isAlwaysBordered == true
                      ? BorderSide(color: AppColor.grey, width: 1)
                      : BorderSide(color: AppColor.editText, width: 0.0),
                )
              : OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: widget.isAlwaysBordered == true
                      ? BorderSide(color: AppColor.grey, width: 1)
                      : BorderSide(color: AppColor.editText, width: 0.0),
                ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(color: AppColor.red, width: 1),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(color: AppColor.red, width: 1),
          ),
          prefixIcon: widget.inputType == InputType.search
              ? Icon(Icons.search, color: AppColor.primary)
              : null,
          suffixIcon: widget.inputType == InputType.password
              ? Container(
                  margin: EdgeInsets.only(right: 15),
                  child: new GestureDetector(
                    onTap: widget.inputType == InputType.password
                        ? () => setState(() => _obscureText = !_obscureText)
                        : null,
                    child: widget.inputType == InputType.password
                        ? new Icon(
                            _obscureText
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: AppColor.editTextIcon,
                          )
                        : SizedBox.shrink(),
                  ),
                )
              : null),
    );
  }
}
