import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_case/data/blocs/bottom_nav_cubit/bottom_nav_cubit.dart';
import 'package:test_case/ui/screens/screens.dart';
import 'package:test_case/utils/colors.dart' as AppColor;

class MainScreen extends StatefulWidget {
  const MainScreen({
    Key key,
  }) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final GlobalKey<ScaffoldState> _scaffoldRootKey =
      new GlobalKey<ScaffoldState>();
  BottomNavCubit _bottomNavCubit;

  @override
  void initState() {
    super.initState();
    _bottomNavCubit = BlocProvider.of<BottomNavCubit>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldRootKey,
        body: BlocBuilder(
          cubit: _bottomNavCubit,
          builder: (context, state) => state == 0
              ? HomeScreen()
              : state == 1
                  ? FavoriteScreen()
                  : state == 2
                      ? ProfileScreen()
                      : SizedBox.shrink(),
        ),
        bottomNavigationBar: BlocBuilder(
          cubit: _bottomNavCubit,
          builder: (context, state) => Theme(
            data: ThemeData(
              splashFactory: InkRipple.splashFactory,
              splashColor: Colors.blue.withOpacity(0.07),
              highlightColor: Colors.blue.withOpacity(0.07),
            ),
            child: BottomNavigationBar(
              backgroundColor: AppColor.bottomNavBg,
              selectedItemColor: AppColor.primary,
              unselectedItemColor: AppColor.bottomNavIconInactive,
              selectedFontSize: 14,
              unselectedFontSize: 12,
              selectedLabelStyle: TextStyle(fontWeight: FontWeight.w700),
              unselectedLabelStyle: TextStyle(fontWeight: FontWeight.w700),
              type: BottomNavigationBarType.fixed,
              elevation: 8,
              onTap: (index) => _bottomNavCubit.navItemTapped(index),
              currentIndex: _bottomNavCubit.state,
              items: _bottomNavCubit.navItem
                  .map(
                    (e) => BottomNavigationBarItem(
                      icon: e.icon,
                      activeIcon: e.activeIcon,
                      label: e.label,
                    ),
                  )
                  .toList(),
            ),
          ),
        ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
