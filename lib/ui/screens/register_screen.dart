import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_case/data/blocs/auth_cubit/auth_cubit.dart';
import 'package:test_case/ui/widgets/edit_text.dart';
import 'package:test_case/ui/widgets/loading_dialog.dart';
import 'package:test_case/ui/widgets/rounded_button.dart';
import 'package:test_case/utils/colors.dart' as AppColor;
import 'package:test_case/utils/extensions.dart' as AppExt;
import 'package:test_case/utils/images.dart' as AppImg;
import 'package:test_case/utils/validator.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final ValidatorCustom _v = ValidatorCustom();
  final _formKey = GlobalKey<FormState>();
  AuthCubit _authCubit;
  TextEditingController _nameController,
      _usernameController,
      _passwordController;

  @override
  void initState() {
    super.initState();
    _authCubit = BlocProvider.of<AuthCubit>(context);
    _nameController = TextEditingController(text: '');
    _usernameController = TextEditingController(text: '');
    _passwordController = TextEditingController(text: '');
  }

  void _handleRegister() async {
    if (_formKey.currentState.validate()) {
      LoadingDialog.show(context);
      await _authCubit.register(
          name: _nameController.text,
          username: _usernameController.text,
          password: _passwordController.text);
      AppExt.popScreen(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => _authCubit,
      child: Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  SizedBox(
                    height: 40,
                  ),
                  Image.asset(
                    AppImg.img_logo,
                    height: 120,
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  EditText(
                    validator: _v.vRequired,
                    autoValidateMode: AutovalidateMode.onUserInteraction,
                    controller: _nameController,
                    hintText: "Name",
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  EditText(
                    validator: _v.vRequired,
                    autoValidateMode: AutovalidateMode.onUserInteraction,
                    controller: _usernameController,
                    hintText: "Username",
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  EditText(
                    validator: _v.vRequired,
                    autoValidateMode: AutovalidateMode.onUserInteraction,
                    controller: _passwordController,
                    inputType: InputType.password,
                    hintText: "Password",
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  RoundedButton.contained(
                    onPressed: _handleRegister,
                    label: "Register",
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        "Have an account  |  ",
                      ),
                      GestureDetector(
                        onTap: () => AppExt.popScreen(context),
                        child: Text(
                          "Login",
                          style: TextStyle(
                            color: AppColor.primary,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
