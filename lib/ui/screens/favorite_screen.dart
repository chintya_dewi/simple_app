import 'dart:async';

import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_case/data/blocs/people/fav_people_cubit/fav_people_cubit.dart';
import 'package:test_case/data/blocs/people/people_cubit/people_cubit.dart';
import 'package:test_case/data/blocs/people/set_fav_people_cubit/set_fav_people_cubit.dart';
import 'package:test_case/ui/widgets/card_people.dart';
import 'package:test_case/ui/widgets/edit_text.dart';
import 'package:test_case/utils/colors.dart' as AppColor;
import 'package:test_case/utils/transitions.dart' as AppTrans;
import 'package:test_case/utils/extensions.dart' as AppExt;

class FavoriteScreen extends StatefulWidget {
  const FavoriteScreen({Key key}) : super(key: key);

  @override
  _FavoriteScreenState createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  FavPeopleCubit _favPeopleCubit;
  SetFavPeopleCubit _setFavPeopleCubit;
  TextEditingController _searchController;
  Timer _debounce;

  @override
  void initState() {
    super.initState();
    _favPeopleCubit = BlocProvider.of<FavPeopleCubit>(context)
      ..showAllFavData();
    _searchController = TextEditingController(text: '');
    _setFavPeopleCubit = SetFavPeopleCubit();
  }

  _onSearchChanged(String keyword) {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      if (keyword.isNotEmpty) {
        _favPeopleCubit.searchFav(keyword.toString());
      } else {
        _favPeopleCubit.showAllFavData();
      }
    });
  }

  @override
  void dispose() {
    _searchController.dispose();
    _setFavPeopleCubit.close();
    if (_debounce?.isActive ?? false) _debounce.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => _favPeopleCubit,
        ),
        BlocProvider(
          create: (_) => _setFavPeopleCubit,
        ),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener(
            cubit: _setFavPeopleCubit,
            listener: (context, state) async {
              if (state is SetFavPeopleFailure) {
                if (state.message != null) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text('Failure add people to favorite'),
                      backgroundColor: AppColor.black,
                    ),
                  );
                }
                return;
              }
              if (state is SetFavPeopleSuccess) {
                await _favPeopleCubit.reloadFavLocalData();
                return;
              }
            },
          ),
        ],
        child: GestureDetector(
          onTap: () => AppExt.hideKeyboard(context),
          child: Scaffold(
            body: SafeArea(
              child: NestedScrollView(
                floatHeaderSlivers: true,
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxIsScrolled) => [
                  SliverAppBar(
                      backgroundColor: Colors.white,
                      snap: true,
                      centerTitle: true,
                      pinned: true,
                      shadowColor: AppColor.black.withOpacity(0.2),
                      floating: true,
                      title: Text("Favorite People"),
                      brightness: Brightness.dark,
                      expandedHeight: (2 * kToolbarHeight),
                      bottom: PreferredSize(
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            height: kToolbarHeight + 15 - 0.7,
                            child: BlocBuilder(
                              cubit: _favPeopleCubit,
                              builder: (context, state) => Center(
                                child: Container(
                                  height: 55,
                                  margin: EdgeInsets.only(top: 3),
                                  child: EditText(
                                    isAlwaysBordered: true,
                                    controller: _searchController,
                                    hintText: "Search favorite people...",
                                    inputType: InputType.search,
                                    onChanged: _onSearchChanged,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          preferredSize: Size.fromHeight(kToolbarHeight + 15))),
                ],
                body: BlocBuilder(
                  cubit: _favPeopleCubit,
                  builder: (context, state) =>
                      AppTrans.SharedAxisTransitionSwitcher(
                    transitionType: SharedAxisTransitionType.vertical,
                    fillColor: Colors.transparent,
                    child: state is FavPeopleLoading
                        ? Center(
                            child: CircularProgressIndicator(),
                          )
                        : state is FavPeopleLoaded
                            ? state.people.length > 0
                                ? ListView.separated(
                                    physics: BouncingScrollPhysics(),
                                    padding:
                                        const EdgeInsets.fromLTRB(0, 20, 0, 20),
                                    itemBuilder: (context, index) => CardPeople(
                                        setFavorite: () async =>
                                            await _setFavPeopleCubit.updateFav(
                                                people: state.people[index]),
                                        people: state.people[index]),
                                    separatorBuilder: (context, index) =>
                                        SizedBox(),
                                    itemCount: state.people.length)
                                : Center(
                                    child: Text("Empty Data"),
                                  )
                            : Center(
                                child: Text("Load Data Failure"),
                              ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
