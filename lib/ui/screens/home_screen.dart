import 'dart:async';

import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:test_case/data/blocs/people/people_cubit/people_cubit.dart';
import 'package:test_case/data/blocs/people/set_fav_people_cubit/set_fav_people_cubit.dart';
import 'package:test_case/ui/screens/people_entry_screen.dart';
import 'package:test_case/ui/widgets/card_people.dart';
import 'package:test_case/ui/widgets/edit_text.dart';
import 'package:test_case/utils/colors.dart' as AppColor;
import 'package:test_case/utils/transitions.dart' as AppTrans;
import 'package:test_case/utils/extensions.dart' as AppExt;

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  PeopleCubit _peopleCubit;
  SetFavPeopleCubit _setFavPeopleCubit;
  TextEditingController _searchController;
  Timer _debounce;

  @override
  void initState() {
    super.initState();
    _peopleCubit = BlocProvider.of<PeopleCubit>(context)..load();
    _searchController = TextEditingController(text: '');
    _setFavPeopleCubit = SetFavPeopleCubit();
  }

  _onSearchChanged(String keyword) {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      if (keyword.isNotEmpty) {
        _peopleCubit.search(keyword.toString());
      } else {
        _peopleCubit.showAllLocalData();
      }
    });
  }

  @override
  void dispose() {
    _searchController.dispose();
    _setFavPeopleCubit.close();
    if (_debounce?.isActive ?? false) _debounce.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => _peopleCubit,
        ),
        BlocProvider(
          create: (_) => _setFavPeopleCubit,
        ),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener(
            cubit: _setFavPeopleCubit,
            listener: (context, state) async {
              if (state is SetFavPeopleFailure) {
                if (state.message != null) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text('Failure add people to favorite'),
                      backgroundColor: AppColor.black,
                    ),
                  );
                }
                return;
              }
              if (state is SetFavPeopleSuccess) {
                await _peopleCubit.reloadLocalData();
                return;
              }
            },
          ),
        ],
        child: GestureDetector(
          onTap: () => AppExt.hideKeyboard(context),
          child: Scaffold(
            body: SafeArea(
              child: NestedScrollView(
                floatHeaderSlivers: true,
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxIsScrolled) => [
                  SliverAppBar(
                      backgroundColor: Colors.white,
                      snap: true,
                      centerTitle: true,
                      pinned: true,
                      shadowColor: AppColor.black.withOpacity(0.2),
                      floating: true,
                      title: Text("People"),
                      brightness: Brightness.dark,
                      expandedHeight: (2 * kToolbarHeight),
                      bottom: PreferredSize(
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            height: kToolbarHeight + 15 - 0.7,
                            child: BlocBuilder(
                              cubit: _peopleCubit,
                              builder: (context, state) => Center(
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        height: 55,
                                        margin: EdgeInsets.only(top: 3),
                                        child: EditText(
                                          isAlwaysBordered: true,
                                          controller: _searchController,
                                          hintText: "Search people...",
                                          inputType: InputType.search,
                                          onChanged: _onSearchChanged,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    IconButton(
                                      padding: EdgeInsets.zero,
                                      iconSize: 22,
                                      splashRadius: 25,
                                      icon: Icon(
                                        state is PeopleLoaded &&
                                                state.sort == SortPeople.desc
                                            ? Boxicons.bx_sort_a_z
                                            : Boxicons.bx_sort_z_a,
                                        color: AppColor.primary,
                                      ),
                                      onPressed: () async =>
                                          await _peopleCubit.sort(),
                                    ),
                                    IconButton(
                                      padding: EdgeInsets.zero,
                                      iconSize: 22,
                                      splashRadius: 25,
                                      icon: Icon(
                                        state is PeopleLoaded &&
                                                state.view == ViewPeople.list
                                            ? Icons.grid_view
                                            : Icons.list,
                                        color: AppColor.primary,
                                      ),
                                      onPressed: () => _peopleCubit.setView(),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          preferredSize: Size.fromHeight(kToolbarHeight + 15))),
                ],
                body: BlocBuilder(
                  cubit: _peopleCubit,
                  builder: (context, state) =>
                      AppTrans.SharedAxisTransitionSwitcher(
                    transitionType: SharedAxisTransitionType.vertical,
                    fillColor: Colors.transparent,
                    child: state is PeopleLoading
                        ? Center(
                            child: CircularProgressIndicator(),
                          )
                        : state is PeopleLoaded
                            ? Stack(
                                children: [
                                  state.people.length > 0
                                      ? state.view == ViewPeople.list
                                          ? ListView.separated(
                                              physics: BouncingScrollPhysics(),
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      0, 20, 0, 60),
                                              itemBuilder: (context, index) =>
                                                  CardPeople(
                                                      setFavorite: () async =>
                                                          await _setFavPeopleCubit
                                                              .updateFav(
                                                                  people: state
                                                                          .people[
                                                                      index]),
                                                      people:
                                                          state.people[index]),
                                              separatorBuilder:
                                                  (context, index) =>
                                                      SizedBox(),
                                              itemCount: state.people.length)
                                          : GridView.builder(
                                              physics: BouncingScrollPhysics(),
                                              gridDelegate:
                                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                                crossAxisCount: 2,
                                                mainAxisSpacing: 13,
                                                crossAxisSpacing: 13,
                                              ),
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      15, 20, 15, 60),
                                              itemCount: state.people.length,
                                              itemBuilder: (BuildContext
                                                          context,
                                                      int index) =>
                                                  CardPeople(
                                                      setFavorite: () async =>
                                                          await _setFavPeopleCubit
                                                              .updateFav(
                                                                  people: state
                                                                          .people[
                                                                      index]),
                                                      isGrid: true,
                                                      people:
                                                          state.people[index]),
                                            )
                                      : Center(
                                          child: Text("Empty Data"),
                                        ),
                                  Positioned(
                                    bottom: 20,
                                    right: 20,
                                    child: FloatingActionButton(
                                      backgroundColor: AppColor.accent,
                                      child: Icon(
                                        Icons.add,
                                        color: AppColor.primary,
                                      ),
                                      onPressed: () => AppExt.pushScreen(
                                        context,
                                        PeopleEntryScreen(),
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            : Center(
                                child: Text("Load Data Failure"),
                              ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
