import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_case/data/blocs/auth_cubit/auth_cubit.dart';
import 'package:test_case/ui/screens/register_screen.dart';
import 'package:test_case/ui/widgets/edit_text.dart';
import 'package:test_case/ui/widgets/loading_dialog.dart';
import 'package:test_case/ui/widgets/rounded_button.dart';
import 'package:test_case/utils/colors.dart' as AppColor;
import 'package:test_case/utils/extensions.dart' as AppExt;
import 'package:test_case/utils/images.dart' as AppImg;
import 'package:test_case/utils/validator.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final ValidatorCustom _v = ValidatorCustom();
  final _formKey = GlobalKey<FormState>();
  AuthCubit _authCubit;
  TextEditingController _usernameController, _passwordController;

  @override
  void initState() {
    super.initState();
    _authCubit = BlocProvider.of<AuthCubit>(context);
    _usernameController = TextEditingController(text: '');
    _passwordController = TextEditingController(text: '');
  }

  void _handleLogin() async {
    if (_formKey.currentState.validate()) {
      LoadingDialog.show(context);
      await _authCubit.login(
          username: _usernameController.text,
          password: _passwordController.text);
      AppExt.popScreen(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => _authCubit,
      child: BlocListener(
        cubit: _authCubit,
        listener: (context, state) {
          if (state is AuthUnauthenticated) {
            if (state.message != null) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text('${state.message}'),
                  backgroundColor: AppColor.black,
                ),
              );
            }
            return;
          }
          if (state is AuthAuthenticated) {
            AppExt.popUntilRoot(context);
            return;
          }
        },
        child: GestureDetector(
          onTap: () => AppExt.hideKeyboard(context),
          child: Scaffold(
            body: SafeArea(
              child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 40,
                      ),
                      Image.asset(
                        AppImg.img_logo,
                        height: 120,
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      EditText(
                        validator: _v.vRequired,
                        autoValidateMode: AutovalidateMode.onUserInteraction,
                        controller: _usernameController,
                        hintText: "Username",
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      EditText(
                        validator: _v.vRequired,
                        autoValidateMode: AutovalidateMode.onUserInteraction,
                        controller: _passwordController,
                        inputType: InputType.password,
                        hintText: "Password",
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      RoundedButton.contained(
                        onPressed: _handleLogin,
                        label: "Login",
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(
                            "Don't have an account  |  ",
                          ),
                          GestureDetector(
                            onTap: () => AppExt.pushScreen(
                              context,
                              RegisterScreen(),
                            ),
                            child: Text(
                              "Register",
                              style: TextStyle(
                                color: AppColor.primary,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
