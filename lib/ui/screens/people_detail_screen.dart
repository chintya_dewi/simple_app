import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_case/data/blocs/people/delete_people/delete_people_cubit.dart';
import 'package:test_case/data/blocs/people/fav_people_cubit/fav_people_cubit.dart';
import 'package:test_case/data/blocs/people/people_cubit/people_cubit.dart';
import 'package:test_case/data/blocs/people/people_detail_cubit/people_detail_cubit.dart';
import 'package:test_case/data/blocs/people/set_fav_people_cubit/set_fav_people_cubit.dart';
import 'package:test_case/data/models/people.dart';
import 'package:test_case/ui/screens/people_entry_screen.dart';
import 'package:test_case/ui/widgets/bottom_sheet_feedback.dart';
import 'package:test_case/ui/widgets/confirmation_dialog.dart';
import 'package:test_case/ui/widgets/loading_dialog.dart';
import 'package:test_case/ui/widgets/rounded_button.dart';
import 'package:test_case/utils/colors.dart' as AppColor;
import 'package:test_case/utils/transitions.dart' as AppTrans;
import 'package:test_case/utils/extensions.dart' as AppExt;
import 'package:flutter_boxicons/flutter_boxicons.dart';

class PeopleDetailScreen extends StatefulWidget {
  const PeopleDetailScreen({Key key, @required this.people}) : super(key: key);
  final People people;

  @override
  _PeopleDetailScreenState createState() => _PeopleDetailScreenState();
}

class _PeopleDetailScreenState extends State<PeopleDetailScreen> {
  PeopleDetailCubit _peopleDetailCubit;
  DeletePeopleCubit _deletePeopleCubit;
  SetFavPeopleCubit _setFavPeopleCubit;

  @override
  void initState() {
    super.initState();
    _peopleDetailCubit = PeopleDetailCubit()..load(widget.people.id);
    _deletePeopleCubit = DeletePeopleCubit();
    _setFavPeopleCubit = SetFavPeopleCubit();
  }

  void _handleDeletePeople() async {
    LoadingDialog.show(context);
    await _deletePeopleCubit.delete(id: widget.people.id);
    AppExt.popScreen(context);
  }

  @override
  void dispose() {
    _deletePeopleCubit.close();
    _setFavPeopleCubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => _peopleDetailCubit,
        ),
        BlocProvider(
          create: (_) => _deletePeopleCubit,
        ),
        BlocProvider(
          create: (_) => _setFavPeopleCubit,
        ),
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener(
            cubit: _deletePeopleCubit,
            listener: (context, state) async {
              if (state is DeletePeopleFailure) {
                if (state.message != null) {
                  BottomSheetFeedback.show(context,
                      color: AppColor.red,
                      icon: Icons.highlight_remove_outlined,
                      title: "Delete People Failure");
                }
                return;
              }
              if (state is DeletePeopleSuccess) {
                await BlocProvider.of<PeopleCubit>(context).reloadLocalData();
                await BlocProvider.of<FavPeopleCubit>(context)
                    .reloadFavLocalData();
                BottomSheetFeedback.show(context,
                    title: "Delete People Success")
                  ..then(
                    (value) => AppExt.popScreen(context),
                  );
                return;
              }
            },
          ),
          BlocListener(
            cubit: _setFavPeopleCubit,
            listener: (context, state) async {
              if (state is SetFavPeopleFailure) {
                if (state.message != null) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text('Failure add people to favorite'),
                      backgroundColor: AppColor.black,
                    ),
                  );
                }
                return;
              }
              if (state is SetFavPeopleSuccess) {
                await _peopleDetailCubit.reload(widget.people.id);
                await BlocProvider.of<PeopleCubit>(context).reloadLocalData();
                await BlocProvider.of<FavPeopleCubit>(context)
                    .reloadFavLocalData();
                return;
              }
            },
          ),
        ],
        child: Scaffold(
          appBar: AppBar(
            shadowColor: Colors.black54,
            backgroundColor: Colors.white,
            centerTitle: true,
            elevation: 0,
            title: Text(
              "People Detail",
              style: TextStyle(fontSize: 18),
            ),
            brightness: Brightness.light,
          ),
          body: SafeArea(
            child: BlocBuilder(
              cubit: _peopleDetailCubit,
              builder: (context, state) =>
                  AppTrans.SharedAxisTransitionSwitcher(
                transitionType: SharedAxisTransitionType.vertical,
                fillColor: Colors.transparent,
                child: state is PeopleDetailLoading
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : state is PeopleDetailLoaded
                        ? SingleChildScrollView(
                            padding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 15),
                            child: Center(
                              child: Column(
                                children: [
                                  Text(
                                    "${state.people.name.capitalize()}",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 25,
                                        fontWeight: FontWeight.w700),
                                  ),
                                  SizedBox(height: 25),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Expanded(
                                        child: Column(
                                          children: [
                                            Text(
                                              "${state.people.height}",
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(
                                              height: 3,
                                            ),
                                            Text(
                                              'Height',
                                              style: TextStyle(
                                                  color:
                                                      AppColor.textSecondary),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Expanded(
                                        child: Column(
                                          children: [
                                            Text(
                                              "${state.people.mass}",
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(
                                              height: 3,
                                            ),
                                            Text(
                                              'Mass',
                                              style: TextStyle(
                                                  color:
                                                      AppColor.textSecondary),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Expanded(
                                        child: Column(
                                          children: [
                                            Text(
                                              "${state.people.birthYear}",
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(
                                              height: 3,
                                            ),
                                            Text(
                                              'Birth Year',
                                              style: TextStyle(
                                                  color:
                                                      AppColor.textSecondary),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 20),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: SizedBox(
                                          child: RoundedButton.outlined(
                                            isSmall: true,
                                            label: "Favorite",
                                            color: AppColor.red,
                                            onPressed: () async =>
                                                await _setFavPeopleCubit
                                                    .updateFav(
                                                        people: state.people),
                                            leading: Icon(
                                              state.people.favorite == 0
                                                  ? Icons.favorite_outline
                                                  : Icons.favorite,
                                              color: AppColor.red,
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      RoundedButton.contained(
                                        isSmall: true,
                                        onPressed: () async {
                                          bool refresh =
                                              await AppExt.pushScreen(
                                                    context,
                                                    PeopleEntryScreen(
                                                      people: state.people,
                                                    ),
                                                  ) ??
                                                  false;
                                          if (refresh) {
                                            _peopleDetailCubit
                                                .reload(widget.people.id);
                                          }
                                        },
                                        leading: Icon(
                                          Icons.edit,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      RoundedButton.contained(
                                        isSmall: true,
                                        color: AppColor.red,
                                        onPressed: () => ConfirmationDialog.show(
                                            context,
                                            onYes: _handleDeletePeople,
                                            desc:
                                                "Are you sure want to delete '${state.people.name}'?"),
                                        leading: Icon(
                                          Icons.delete,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 15),
                                  Divider(),
                                  SizedBox(height: 20),
                                  _DetailItem(
                                      color: AppColor.accent.withOpacity(0.75),
                                      title:
                                          "${state.people.gender.capitalize()}",
                                      icon: Icon(state.people.gender
                                                  .toLowerCase() ==
                                              "female"
                                          ? Boxicons.bx_female_sign
                                          : state.people.gender.toLowerCase() ==
                                                  "male"
                                              ? Boxicons.bx_male_sign
                                              : Icons.remove),
                                      desc: "Gender"),
                                  SizedBox(height: 15),
                                  _DetailItem(
                                      color: AppColor.softGrey,
                                      title:
                                          "${state.people.hairColor.capitalize()}",
                                      icon: Icon(Icons.face),
                                      desc: "Hair Color"),
                                  SizedBox(height: 15),
                                  _DetailItem(
                                      color: AppColor.accent.withOpacity(0.75),
                                      title:
                                          "${state.people.eyeColor.capitalize()}",
                                      icon: Icon(Icons.remove_red_eye),
                                      desc: "Eye Color"),
                                  SizedBox(height: 15),
                                  _DetailItem(
                                      color: AppColor.softGrey,
                                      title:
                                          "${state.people.skinColor.capitalize()}",
                                      icon: Icon(
                                        Boxicons.bxs_t_shirt,
                                        size: 22,
                                      ),
                                      desc: "Skin Color"),
                                  SizedBox(height: 15),
                                ],
                              ),
                            ),
                          )
                        : state is PeopleDetailLoadFailure
                            ? Center(
                                child: Text("Load Data Failure"),
                              )
                            : SizedBox.shrink(),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _DetailItem extends StatelessWidget {
  const _DetailItem(
      {Key key,
      @required this.title,
      @required this.icon,
      @required this.desc,
      this.color})
      : super(key: key);
  final String title;
  final Icon icon;
  final String desc;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: color ?? AppColor.primary,
      ),
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: AppColor.primary.withOpacity(0.25),
            ),
            padding: EdgeInsets.all(7),
            child: icon ?? SizedBox(),
          ),
          SizedBox(
            width: 15,
          ),
          Expanded(
            child: Text(
              title ?? '',
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
            ),
          ),
          SizedBox(
            width: 15,
          ),
          Text(
            desc ?? '',
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
          ),
        ],
      ),
    );
  }
}
