import 'package:flutter/material.dart';
import 'package:test_case/utils/images.dart' as AppImg;

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Image.asset(
            AppImg.img_logo,
            height: 120,
          ),
        ),
      ),
    );
  }
}
