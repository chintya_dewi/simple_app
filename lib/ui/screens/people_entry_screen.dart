import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_case/data/blocs/people/people_cubit/people_cubit.dart';
import 'package:test_case/data/blocs/people/people_entry_cubit/people_entry_cubit.dart';
import 'package:test_case/data/models/people.dart';
import 'package:test_case/ui/widgets/bottom_sheet_feedback.dart';
import 'package:test_case/ui/widgets/confirmation_dialog.dart';
import 'package:test_case/ui/widgets/dropdown_input.dart';
import 'package:test_case/ui/widgets/edit_text.dart';
import 'package:test_case/ui/widgets/loading_dialog.dart';
import 'package:test_case/ui/widgets/rounded_button.dart';
import 'package:test_case/utils/colors.dart' as AppColor;
import 'package:test_case/utils/extensions.dart' as AppExt;
import 'package:test_case/utils/validator.dart';

class PeopleEntryScreen extends StatefulWidget {
  const PeopleEntryScreen({Key key, this.people}) : super(key: key);
  final People people;

  @override
  _PeopleEntryScreenState createState() => _PeopleEntryScreenState();
}

class _PeopleEntryScreenState extends State<PeopleEntryScreen> {
  final ValidatorCustom _v = ValidatorCustom();
  final _formKey = GlobalKey<FormState>();
  PeopleEntryCubit _peopleEntryCubit;
  TextEditingController _nameController,
      _heightController,
      _massController,
      _birthController,
      _eyeController,
      _hairController,
      _skinController;

  bool _isUpdateEntry() => widget.people != null;
  List<String> _gender = ["female", "male", "n/a"];
  String _selectedGender;

  @override
  void initState() {
    super.initState();
    _selectedGender = _isUpdateEntry()
        ? '${widget.people.gender.toLowerCase() != 'female' || widget.people.gender.toLowerCase() != 'male' ? 'n/a' : widget.people.gender.toLowerCase()}'
        : 'n/a';
    _peopleEntryCubit = PeopleEntryCubit();
    _nameController = TextEditingController(
        text: _isUpdateEntry() ? '${widget.people.name}' : '');
    _heightController = TextEditingController(
        text: _isUpdateEntry() ? '${widget.people.height}' : '');
    _massController = TextEditingController(
        text: _isUpdateEntry() ? '${widget.people.mass}' : '');
    _birthController = TextEditingController(
        text: _isUpdateEntry() ? '${widget.people.birthYear}' : '');
    _eyeController = TextEditingController(
        text: _isUpdateEntry() ? '${widget.people.eyeColor}' : '');
    _hairController = TextEditingController(
        text: _isUpdateEntry() ? '${widget.people.hairColor}' : '');
    _skinController = TextEditingController(
        text: _isUpdateEntry() ? '${widget.people.skinColor}' : '');
  }

  void _handleSave() async {
    if (_formKey.currentState.validate()) {
      LoadingDialog.show(context);
      if (!_isUpdateEntry()) {
        await _peopleEntryCubit.insert(
          people: People(
            name: _nameController.text,
            height: _heightController.text,
            mass: _massController.text,
            hairColor: _hairController.text,
            skinColor: _skinController.text,
            eyeColor: _eyeController.text,
            gender: _selectedGender,
            birthYear: _birthController.text,
            created: DateTime.now(),
            edited: DateTime.now(),
            favorite: 0,
          ),
        );
      } else {
        await _peopleEntryCubit.update(
          people: People(
            id: widget.people.id,
            name: _nameController.text,
            height: _heightController.text,
            mass: _massController.text,
            hairColor: _hairController.text,
            skinColor: _skinController.text,
            eyeColor: _eyeController.text,
            gender: _selectedGender,
            birthYear: _birthController.text,
            created: widget.people.created,
            edited: DateTime.now(),
            favorite: widget.people.favorite,
          ),
        );
      }
      AppExt.popScreen(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => _peopleEntryCubit,
      child: BlocListener(
        cubit: _peopleEntryCubit,
        listener: (context, state) async {
          if (state is PeopleEntryFailure) {
            if (state.message != null) {
              BottomSheetFeedback.show(context,
                  color: AppColor.red,
                  icon: Icons.highlight_remove_outlined,
                  title:
                      "${_isUpdateEntry() ? 'Edit People' : 'Add People'} Failure");
            }
            return;
          }
          if (state is PeopleEntrySuccess) {
            await BlocProvider.of<PeopleCubit>(context).showAllLocalData();
            BottomSheetFeedback.show(context,
                title:
                    "${_isUpdateEntry() ? 'Edit People' : 'Add People'} Success")
              ..then(
                (value) => AppExt.popScreen(context, true),
              );
            return;
          }
        },
        child: GestureDetector(
          onTap: () => AppExt.hideKeyboard(context),
          child: Scaffold(
            appBar: AppBar(
              shadowColor: Colors.black54,
              backgroundColor: Colors.white,
              centerTitle: true,
              elevation: 0,
              title: Text(
                !_isUpdateEntry() ? "Add People " : "Edit People",
                style: TextStyle(fontSize: 18),
              ),
              brightness: Brightness.light,
            ),
            body: SafeArea(
              child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Name",
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      EditText(
                        validator: _v.vRequired,
                        autoValidateMode: AutovalidateMode.onUserInteraction,
                        controller: _nameController,
                        hintText: "",
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Height",
                                  style: TextStyle(fontSize: 16),
                                ),
                                SizedBox(
                                  height: 7,
                                ),
                                EditText(
                                  validator: _v.vRequired,
                                  autoValidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  keyboardType: TextInputType.number,
                                  controller: _heightController,
                                  hintText: "",
                                ),
                              ],
                            ),
                          ),
                          SizedBox(width: 10),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Mass",
                                  style: TextStyle(fontSize: 16),
                                ),
                                SizedBox(
                                  height: 7,
                                ),
                                EditText(
                                  validator: _v.vRequired,
                                  autoValidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  keyboardType: TextInputType.number,
                                  controller: _massController,
                                  hintText: "",
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Gender",
                                  style: TextStyle(fontSize: 16),
                                ),
                                SizedBox(
                                  height: 7,
                                ),
                                DropdownInput(
                                  isEmpty: _selectedGender == 'n/a',
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton<String>(
                                      dropdownColor: Colors.white,
                                      value: _selectedGender,
                                      isDense: true,
                                      onChanged: (String newValue) {
                                        setState(() {
                                          _selectedGender = newValue;
                                        });
                                      },
                                      items: _gender.map((String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(width: 10),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Birth Year",
                                  style: TextStyle(fontSize: 16),
                                ),
                                SizedBox(
                                  height: 7,
                                ),
                                EditText(
                                  validator: _v.vRequired,
                                  autoValidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  keyboardType: TextInputType.visiblePassword,
                                  controller: _birthController,
                                  hintText: "",
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Eye Color",
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      EditText(
                        validator: _v.vRequired,
                        autoValidateMode: AutovalidateMode.onUserInteraction,
                        controller: _eyeController,
                        hintText: "",
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Hair Color",
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      EditText(
                        validator: _v.vRequired,
                        autoValidateMode: AutovalidateMode.onUserInteraction,
                        controller: _hairController,
                        hintText: "",
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Skin Color",
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      EditText(
                        validator: _v.vRequired,
                        autoValidateMode: AutovalidateMode.onUserInteraction,
                        controller: _skinController,
                        hintText: "",
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      RoundedButton.contained(
                        onPressed: _isUpdateEntry()
                            ? () => ConfirmationDialog.show(
                                  context,
                                  desc:
                                      "Are you sure data is correct and want to change '${widget.people.name}' data? ",
                                  onYes: () => _handleSave(),
                                )
                            : _handleSave,
                        label: "Save",
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
