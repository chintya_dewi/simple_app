import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boxicons/flutter_boxicons.dart';
import 'package:test_case/data/blocs/auth_cubit/auth_cubit.dart';
import 'package:test_case/data/blocs/bottom_nav_cubit/bottom_nav_cubit.dart';
import 'package:test_case/ui/widgets/rounded_button.dart';
import 'package:test_case/utils/colors.dart' as AppColor;
import 'package:test_case/utils/extensions.dart' as AppExt;

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.black54,
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 0,
        title: Text(
          "Profile",
          style: TextStyle(fontSize: 18),
        ),
        brightness: Brightness.light,
      ),
      body: BlocBuilder(
        cubit: BlocProvider.of<AuthCubit>(context),
        builder: (context, state) => SafeArea(
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(20.0),
            child: state is AuthAuthenticated
                ? Column(
                    children: [
                      SizedBox(
                        height:30,
                      ),
                      Icon(
                        Boxicons.bx_user,
                        size: 60,
                        color: AppColor.grey,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        "${state.user.name.capitalize()}",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.w700),
                      ),
                      SizedBox(height: 5),
                      Text(
                        "@${state.user.username}",
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            color: AppColor.textSecondary),
                      ),
                      SizedBox(height: 40),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: RoundedButton.outlined(
                          label: "Logout",
                          color: AppColor.red,
                          onPressed: () {
                            BlocProvider.of<BottomNavCubit>(context)
                                .navItemTapped(0);
                            BlocProvider.of<AuthCubit>(context).logout();
                          },
                        ),
                      ),
                    ],
                  )
                : SizedBox.shrink(),
          ),
        ),
      ),
    );
  }
}
