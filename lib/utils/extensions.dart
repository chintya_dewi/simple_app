import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

extension StringExtension on String {
  String capitalize() {
    String _result = "";
    List<String> _splitted = this.split(' ');
    for (var i = 0; i < _splitted.length; i++) {
      _result = _result +
          "${_splitted[i][0].toUpperCase()}${_splitted[i].substring(1)}";
      if (i != _splitted.length - 1) {
        _result = _result + " ";
      }
    }
    return _result;
  }
}

void backToRoot(context) {
  Navigator.pushNamedAndRemoveUntil(context, '/', (_) => false);
}

void popUntilRoot(context) {
  Navigator.popUntil(context, ModalRoute.withName('/'));
}

void backToMain(context) {
  Navigator.pushNamedAndRemoveUntil(context, '/main', (_) => false);
}

void hideKeyboard(context) {
  FocusScope.of(context).requestFocus(FocusNode());
}

void popScreen(BuildContext context, [dynamic data]) {
  Navigator.pop(context, data);
}

enum RouteTransition { slide, dualSlide, fade, material, cupertino, slideUp }

Future pushScreen(BuildContext context, Widget buildScreen) async {
  dynamic data;
  data = await Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => buildScreen),
  );

  return data;
}

void pushAndRemoveScreen(BuildContext context, {@required Widget pageRef}) {
  Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => pageRef),
      (Route<dynamic> route) => false);
}

Color computeTextColor(Color color) {
  return color.computeLuminance() > 0.5 ? Colors.black : Colors.white;
}
