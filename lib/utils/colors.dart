import 'dart:ui';
import 'package:flutter/material.dart';

// main color
const primary = Color(0xFF333652);
const accent = Color(0xFFFAD02C);
const primaryInverted = Color(0xFFFFFFFF);

const black = Color(0xFF21232C);
const grey = Color(0xFF9D9D9D);
const red = Color(0xFFE64036);
const green = Color(0xFF479E10);
const softBlue = Color(0xFFC0D1E5);
const softOrange = Color(0xFFF9CFB1);
const softGrey = Color(0xFFE9EAEC);

// edit text
const editTextCursor = Color(0xFFB8B8B8);
const editText = Color(0xFFF4F4F4);
const editTextField = Color(0xFFE7E7E7);
const editTextIcon = Color(0xFF8F9497);

// button
const roundedButtonDisabled = Color(0xFFF4F4F4);

// text
const textPrimary = Color(0xFF21232C);
const textPrimaryInverted = Color(0xFFFFFFFF);
const textSecondary = Color(0xFFBEBEBE);

// line
const line = Color(0xFFC4C4C4);

// bottom nav
const bottomNavIconActive = Color(0xFF4BAA0F);
const bottomNavIconInactive = Color(0xFFBEBEBE);
const bottomNavBg = Color(0xFFFFFFFF);
